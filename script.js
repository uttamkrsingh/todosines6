const $ = (element) => { return document.createElement(element)}        // create element
const createNode = (text) => { return document.createTextNode(text)}     // for create text node 

const addClass = (element,classes) => {                         // for adding classes in element
    //console.log(element);
    for(var i = 0; i<classes.length ; i++){
        element.classList.add(classes[i]);
    }
    
}

const append = (parent,child) => { return parent.appendChild(child)}  // for append element in elemnet.
 
const pageLoad = () => {
    let mainbody = document.getElementById('containerBody');
   
    // for inputbox
   let inputArea = $('div');
  
   addClass(inputArea,['row','form-group']);
   let inputTextArea = $('div');
   addClass(inputTextArea,['col-md-6']);
   let inputText = $('input');
   addClass(inputText,['form-control']);
   inputText.setAttribute('id','inputData')
   inputText.setAttribute('placeholder', 'Type your task here....');
   append(inputTextArea,inputText);
   append(inputArea,inputTextArea);
   append(mainbody,inputArea);
    // for submit button
    let buttonArea = $('div');
    addClass(buttonArea,['col-md-2']);
    let button = $('button');
    addClass(button,['btn','btn-primary','btn-block']);
    let text = createNode('Add Todo');
    button.setAttribute('id','saveBtn')
    append(button,text);
    append(buttonArea,button);
    append(inputArea,buttonArea);

    // for table area
    let table = $('table');
    addClass(table,['table']);
    table.setAttribute('id','todoList')
    let tr = $("tr");
    //th create
    let th1 = $('th');
    let th2 =$('th');
    let th1Text = createNode('Title');
    let th2Text = createNode('Action');
    append(th1,th1Text);
    append(th2,th2Text);
    append(tr,th1);
    append(tr,th2);
    append(table,tr);
    append(mainbody,table); 
    
    let todos = [
        {taskTitle: 'Sample task one', completed: false},
        {taskTitle: 'Sample task two', completed: false}
    ];
    // find data in todos array
    let showList = () => {
        let tbl = document.getElementById("todoList");
        tbl.innerHTML= "";
        for(var i = 0; i<todos.length; i++){
            var todo = todos[i];
            addInList(todo);
            //console.log(todos.length);
        }
    }
    
    // create view list in table
    let addInList = (todo) => {
        
        let remove = () => {
            let id = but2.getAttribute('data-id');
             todos.splice(id, 1);           
            showList();
        }

        let tbl = document.getElementById("todoList");
        //console.log(todos.indexOf(todo));
        let tr2 = $("tr");
        let td1 = $('td');
        let td2 =$('td');
        let td1Text = createNode(todo.taskTitle);
        let but1 = $('button');
        addClass(but1, ['btn','btn-info','m-1'])
        let but1Text = createNode('Undone');
        let but2 = $('button');
        addClass(but2, ['btn','btn-danger','m-1'])
        let but2Text = createNode('Delete');
        but2.addEventListener('click', remove);
        but2.setAttribute('data-id', todos.indexOf(todo));
        append(td1,td1Text);
        append(but1,but1Text);
        append(td2,but1);
        append(but2,but2Text);
        append(td2,but2);
        append(tr2,td1);
        append(tr2,td2);
        append(table,tr2);
        append(mainbody,tbl);  
               
    }

    // adding data in table
    let addTodo = () => {
        let data = document.getElementById('inputData');
        todos.unshift({
            taskTitle : data.value,
            completed : false
        })
        console.log(todos)
        showList();
        data.value = '';
        data.focus();
    }

    // addind data on click event
    let btn = document.getElementById('saveBtn');
    btn.addEventListener('click', () => {
        addTodo();
    });

    // remove data from table

    showList();
    
}

pageLoad();
